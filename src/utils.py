import pandas as pd


## Extracción
def group_column(data, name, group=None, func='count'):
	"""
	función para hacer agregaciones a un dataframe
	:param data: dataframe
	:param name:  string, nombre para la nueva columna
	:param group: lista o string, nombre de las columnas para agrupar
	:param func: string o numpy ej: np.sum, funcion de agregacion
	:return:
	"""
	default=['Día']
	if group:
		if type(group) == list:
			default.extend(group)
		else:
			default.append(group)
	temp = data.groupby(default).agg({'Caso Hurto': func})
	temp.columns = [name]
	df = temp.reset_index()
	df = pd.merge(data, df, how='left', on=default)
	return df, temp

# convert series to supervised learning
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = pd.DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg